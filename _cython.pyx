import knapsack

def knap():

    cdef int size[8]
    cdef int weight[8]

    size = [21, 11, 15, 9, 34, 25, 41, 55]
    weight = [22, 12, 16, 10, 35, 26, 42, 53]

    capacity = 100

    return knapsack.knapsack(size, weight).solve(capacity)