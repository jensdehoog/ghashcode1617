"""

The code to be executed by Cython needs to be written in the file '_cython.pyx'.
That code will be compiled, after which it can be executed by this module.

---

Usage of this module:
    import cythonize

    cythonize.your_custom_function()
"""

print("[INFO] Cythonizing...")

import pyximport
pyximport.install()
import _cython

print("[INFO] Done cythonizing!")


"""
Define here your custom functions, which are calling the functions of the cython file.

| | |
v v v
"""


def knap():
    return _cython.knap()